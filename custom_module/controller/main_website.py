from odoo import http
from odoo.http import request
from odoo.exceptions import AccessError, MissingError

class Sale(http.Controller):
	#type ="http" , "json" , auth="public",'users','none'

	@http.route('/my_sale_details',type="http",auth="public",website=True)
	def get_sale_orders(self, **kwargs):
		sale_details = request.env['sale.order'].sudo().search([])

		#return {'my_details':sale_details}

		return request.render('custom_module.sale_details_page',{'my_details':sale_details})

	@http.route('/new_request_submit',type="http",auth="public",website=True,csrf=True)
	def request_submit(self, **kwargs):
		student_name = kwargs.get('st_name')
		
		stu = request.env['student.student'].sudo().create({
			'name':student_name,
			'amount':900,
			'is_student':True

			})

		#sale_details = request.env['student.student'].sudo().search([('id','=',stu)])
		#return request.render('custom_module.sale_details_page',{'my_details':sale_details})